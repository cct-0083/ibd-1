﻿-- povoando as tabelas
-- tabela cliente primeiro cliente
insert into cliente(nome,endereco) values('Aline','Rua-2');
insert into compra(data,total,codclie) values('2019-10-08',345.46,currval('cliente_codigo_seq')),
('2018-01-01',543.35,currval('cliente_codigo_seq'));

-- tabela cliente segundo cliente
insert into cliente(nome,endereco) values('Rubens','Rua-10');
insert into compra(data,total,codclie) values('2019-05-03',45.75,currval('cliente_codigo_seq')),('2019-02-20',200.37,currval('cliente_codigo_seq'));

-- reseta os sequenciadores
select setval('cliente_codigo_seq',1,false); -- false significa que inicia em 1, true, no próximo valor
select setval('compra_codigo_seq',1,false);

-- Outra forma de fazer isso 
-- Aqui você trunca (apaga) os valores e exclui em cascata.
truncate table compra, cliente restart identity;
-- ou
truncate table cliente restart identity cascade;

-- os sequenciadores podem usar as seguintes funcoes: setval, nextval, currval
-- setval -> seta o valor
-- nextval -> retorna o proximo valor
-- currval -> retorna o valor corrente