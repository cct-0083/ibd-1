﻿-- RIR = Regra de Integridade Referencial
-- Duas regras:
-- A chave estrangeira de uma tabela pode ser igual a NULL se os atributos da chave estrangeira não pertencem a chave primária da própria tabela
-- A chave estrangeira pode ser diferente de NULL se existir o mesmo valor na chave primária referenciada

update compra 
set codclie=null
where numero =2 
-- Essa atualização vai funcionar porque a chave estrageira da tabela compra não faz parte da chave primária de compra

update compra
set codclie=4
where numero=2
-- Essa atualização não vai funcionar porque não existe um valor de chave primária equivalente na tabela cliente