﻿-- Script de manutencao

DROP TABLE IF EXISTS public.compra;
DROP TABLE IF EXISTS public.cliente;

-- Teste se as tabelas compra e cliente existem no schema public 
-- Schema public é padrão

CREATE TABLE CLIENTE (
  codigo serial not null,
  nome varchar(60) not null,
  endereco varchar(60) not null,
  constraint codigo_pk primary key (codigo));

CREATE TABLE COMPRA (
  codigo serial not null,
  data date,
  total numeric(10,2), -- equivale a decimal
  codclie integer,
  constraint compra_pk primary key (codigo),	
  constraint compra_fk foreign key (codclie) references cliente(codigo));  

