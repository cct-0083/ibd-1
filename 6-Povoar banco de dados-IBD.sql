﻿-- povoando as tabelas
-- tabela cliente
insert into cliente(nome,endereco) values('Aline','Rua-2');
insert into cliente(nome,endereco) values('Rubens','Rua-10');
-- tabela compras
insert into compra(data,total,codclie) values('2019-10-08',345.46,1),('2018-01-01',543.35,1),('2017-09-10',245.00,1);
insert into compra(data,total,codclie) values('2019-05-03',45.75,2),('2019-02-20',200.37,2);

-- reseta os sequenciadores
select setval('cliente_codigo_seq',1,false); -- false significa que inicia em 1, true, no próximo valor
select setval('compra_codigo_seq',1,false);

-- Outra forma de fazer isso 
-- Aqui você trunca (apaga) os valores e exclui em cascata.
truncate table compra, cliente restart identity;
-- ou
truncate table cliente restart identity cascade;